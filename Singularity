Bootstrap: docker
From: ubuntu

%files
    ## We copy the files belonging to the planner to a dedicated folder in the image.
	. /planner

%post

    ## The "%post"-part of this script is called after the container has
    ## been created with the "%setup"-part above and runs "inside the
    ## container". Most importantly, it is used to install dependencies
    ## and build the planner. Add all commands that have to be executed
    ## once before the planner runs in this part of the script.

    ## Install all necessary dependencies.
    apt-get update
    apt-get -y install openjdk-8-jdk gradle

    ## go to directory and make the planner
    cd /planner
    ./gradlew build -PnoTest


%runscript
    ## The runscript is called whenever the container is used to solve
    ## an instance.

    DOMAINFILE=$1
    PROBLEMFILE=$2
    PLANFILE=$3
    TIMELIMIT=$4
    MEMORYLIMIT=$5

    stdbuf -o0 -e0 java -server -Xms${MEMORYLIMIT}m -Xmx${MEMORYLIMIT}m -cp /planner/build/libs/pddl4j-3.8.3.jar fr.uga.pddl4j.planners.htn.stn.tfd.TFDPlanner -d $DOMAINFILE -p $PROBLEMFILE -l 9 -t $TIMELIMIT 2>&1 | tee $PLANFILE


## Update the following fields with meta data about your submission.
## Please use the same field names and use only one line for each value.
%labels
Name        PDDL4J
Description PDDL4J
Authors     Damien Pellier <damien.pellier@imag.fr>
SupportsRecursion yes
SupportsPartialOrder no
